import { combineReducers } from "redux";
import textInputCheckerReducer from '../Reducer/TextInputCheckerReducer';

export default combineReducers({
    textInputCheckerReducer
})
