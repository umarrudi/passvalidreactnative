import { PASSWORD_CHANGED } from '../Action/types';
const initialState = { password: '' }

export default function textInputCheckerReducer (state= initialState, action){
    console.log(action);
    switch(action.type) {
        case PASSWORD_CHANGED:
            return { ...state, password: action.payload }
        default: 
            return state
    }
}